# Sistema Operativo GNU/Linux para la Administración Pública del Gobierno de Misiones

Distribución del Sistema Operativo GNU/Linux adaptado a las necesidades de las oficinas administrativas de la administración pública provincial, incorporando herramientas de Software Libre.

Basado en el Sistema Operativo **Devuan (Debian)**, contiene herramientas de uso habitual en las oficinas gubernamentales, cubriendo todas las necesidades informáticas de la administración pública.

Incluye **LibreOffice** como herramienta ofimática, **Firefox** como navegador de internet, **Evolution** para la gestión del correo, **GIMP** como editor de gráficos, **VLC** como reproductor multimedia,

![Captura de Pantalla](docs/imagenes/inst-22.png)
