export GNUPGHOME="$HOME/.gnupg"

clear
logger -t repositorio[$$] Actualizando el repositorio Devuan 3 Beowulf
echo  Actualizando el repositorio Devuan 3 Beowulf
echo  --------------------------------------------
sleep 10

debmirror /srv/mirror/devuan \
 --keyring=/usr/share/keyrings/devuan-archive-keyring.gpg \
 --ignore-release-gpg \
 --ignore-missing-release \
 --nosource \
 --host=devuan.c3sl.ufpr.br \
 --allow-dist-rename \
 --root=merged \
 --dist=beowulf,beowulf-updates,beowulf-security,beowulf-proposed-updates,beowulf-backports \
 --section=main,contrib,non-free \
 --i18n \
 --arch=amd64 \
 --passive  \
 --cleanup \
 --postcleanup  \
 --diff=none \
 --progress  \
 --verbose \
 --omit-suite-symlinks \
 --method=http \
 --rsync-options="-aIL --partial"  \
 --user=anonymous  \
 --passwd=anonymous@  \
 --exclude-deb-section=cli-mono  \
 --exclude-deb-section=education  \
 --exclude-deb-section=electronics  \
 --exclude-deb-section=embedded  \
 --exclude-deb-section=games  \
 --exclude-deb-section=gnu-r  \
 --exclude-deb-section=gnustep  \
 --exclude-deb-section=hamradio  \
 --exclude-deb-section=haskell  \
 --exclude-deb-section=interpreters  \
 --exclude-deb-section=introspection  \
 --exclude-deb-section=kde  \
 --exclude-deb-section=libdevel  \
 --exclude-deb-section=lisp  \
 --exclude-deb-section=math  \
 --exclude-deb-section=metapackages  \
 --exclude-deb-section=news  \
 --exclude-deb-section=ocaml  \
 --exclude-deb-section=perl  \
 --exclude-deb-section=php  \
 --exclude-deb-section=ruby  \
 --exclude-deb-section=rust  \
 --exclude-deb-section=science  \
 --exclude-deb-section=tasks  \
 --exclude-deb-section=tex  \
 --exclude-deb-section=vcs  \
 --exclude-deb-section=video  \
 --exclude-deb-section=virtual  \
 --exclude-deb-section=zope

logger -t repositorio[$$] Finalizada la actualizacion de repositorio Devuan 3


clear
logger -t repositorio[$$] Actualizando el repositorio Devuan 4 Chimaera
echo  Actualizando el repositorio Devuan 4 Chimaera 
echo  ---------------------------------------------
sleep 10

debmirror /srv/mirror/devuan4 \
 --keyring=/usr/share/keyrings/devuan-archive-keyring.gpg \
 --ignore-release-gpg \
 --ignore-missing-release \
 --nosource \
 --host=devuan.c3sl.ufpr.br \
 --allow-dist-rename \
 --root=merged \
 --dist=chimaera,chimaera-updates,chimaera-security,chimaera-proposed-updates,chimaera-backports \
 --section=main,contrib,non-free \
 --i18n \
 --arch=amd64 \
 --passive  \
 --cleanup \
 --postcleanup  \
 --diff=none \
 --progress  \
 --verbose \
 --omit-suite-symlinks \
 --method=http \
 --rsync-options="-aIL --partial"  \
 --user=anonymous  \
 --passwd=anonymous@  \
 --exclude-deb-section=cli-mono  \
 --exclude-deb-section=education  \
 --exclude-deb-section=electronics  \
 --exclude-deb-section=embedded  \
--i18n \
 --arch=amd64 \
 --passive  \
 --cleanup \
 --postcleanup  \
 --diff=none \
 --progress  \
 --verbose \
 --omit-suite-symlinks \
 --method=http \
 --rsync-options="-aIL --partial"  \
 --user=anonymous  \
 --passwd=anonymous@  \
 --exclude-deb-section=cli-mono  \
 --exclude-deb-section=education  \
 --exclude-deb-section=electronics  \
 --exclude-deb-section=embedded  \
 --exclude-deb-section=games  \
 --exclude-deb-section=gnu-r  \
 --exclude-deb-section=gnustep  \
 --exclude-deb-section=hamradio  \
 --exclude-deb-section=haskell  \
 --exclude-deb-section=interpreters  \
 --exclude-deb-section=introspection  \
 --exclude-deb-section=kde  \
 --exclude-deb-section=libdevel  \
 --exclude-deb-section=lisp  \
 --exclude-deb-section=math  \
 --exclude-deb-section=metapackages  \
 --exclude-deb-section=news  \
 --exclude-deb-section=ocaml  \
 --exclude-deb-section=perl  \
 --exclude-deb-section=php  \
 --exclude-deb-section=ruby  \
 --exclude-deb-section=rust  \
 --exclude-deb-section=science  \
 --exclude-deb-section=tasks  \
 --exclude-deb-section=tex  \
 --exclude-deb-section=vcs  \
 --exclude-deb-section=video  \
 --exclude-deb-section=virtual  \
 --exclude-deb-section=zope

logger -t repositorio[$$] Finalizada la actualizacion de repositorio Devuan 4

