#   ██████╗  ██████╗ ██████╗ ███╗   ███╗██╗███████╗                         
#  ██╔════╝ ██╔═══██╗██╔══██╗████╗ ████║██║██╔════╝                         
#  ██║  ███╗██║   ██║██████╔╝██╔████╔██║██║███████╗                         
#  ██║   ██║██║   ██║██╔══██╗██║╚██╔╝██║██║╚════██║                         
#  ╚██████╔╝╚██████╔╝██████╔╝██║ ╚═╝ ██║██║███████║                         
#   ╚═════╝  ╚═════╝ ╚═════╝ ╚═╝     ╚═╝╚═╝╚══════╝                         
#   ██████╗ ███╗   ██╗██╗   ██╗    ██╗██╗     ██╗███╗   ██╗██╗   ██╗██╗  ██╗
#  ██╔════╝ ████╗  ██║██║   ██║   ██╔╝██║     ██║████╗  ██║██║   ██║╚██╗██╔╝
#  ██║  ███╗██╔██╗ ██║██║   ██║  ██╔╝ ██║     ██║██╔██╗ ██║██║   ██║ ╚███╔╝ 
#  ██║   ██║██║╚██╗██║██║   ██║ ██╔╝  ██║     ██║██║╚██╗██║██║   ██║ ██╔██╗ 
#  ╚██████╔╝██║ ╚████║╚██████╔╝██╔╝   ███████╗██║██║ ╚████║╚██████╔╝██╔╝ ██╗
#   ╚═════╝ ╚═╝  ╚═══╝ ╚═════╝ ╚═╝    ╚══════╝╚═╝╚═╝  ╚═══╝ ╚═════╝ ╚═╝  ╚═╝
#                                                                           

clear

echo  GobMis GNU-Linux 
echo  Actualizacion Post-instalación del sistema    
echo =============================================
echo 


cd ~

if [ -d trabajo ]; then
    mkdir trabajo
else    
    cd trabajo
    
    # Reconfiguracion de los repositorios
    wget http://distro.misiones.gob.ar/arreglos/sources.list
    hoy=$(date +"%Y-%m-%d-%S")
    archivo_copia="/etc/apt/sources.list.$hoy.bak"
    sudo cp /etc/apt/sources.list $archivo_copia
    sudo mv sources.list /etc/apt/sources.list

    # Parche acumulativo de correcciones post-ISO
    wget http://distro.misiones.gob.ar/arreglos/parche.deb
      if [ -f parche.deb ]; then
        sudo apt install ./parche.deb
        rm parche.deb
    fi

fi


# Actualiza todo el sistema

sudo apt update; sudo apt install -f -y; sudo apt full-upgrade -y; sudo apt get clean; sudo apt autoclean; sudo apt autoremove -y; sudo apt-get  purge
echo  *
echo  * Fin de la actualizacion


    
