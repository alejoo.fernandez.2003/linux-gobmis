Instalación en el disco
=======================

Inserte el medio de arranque ya sea en la lectora de DVD o en una ranura USB, reinicie la computadora y seleccione en el BIOS que el arranque se hará por el dispositivo que eligió.

.. figure:: imagenes/inst-01.png
    :width: 600px
    :align: center

**¡Atención!**
    Guarde adecuadamente la clave del Superusuario (*root*) porque es el único que puede realizar tareas de mantenimiento del sistema. 
    
    El usuario común tiene restringido el acceso a las tareas de administración.


.. figure:: imagenes/inst-02.png
    :width: 600px
    :align: center

.. figure:: imagenes/inst-03.png
    :width: 600px
    :align: center

.. figure:: imagenes/inst-04.png
    :width: 600px
    :align: center

.. figure:: imagenes/inst-05.png
    :width: 600px
    :align: center

.. figure:: imagenes/inst-06.png
    :width: 600px
    :align: center

.. figure:: imagenes/inst-07.png
    :width: 600px
    :align: center

.. hint::
    Para organizar las particiones del disco de forma eficiente, recomendamos separar la partición donde se aloja el sistema de los datos del usuario. Elija la opción "**Separar la partición home**". El instalador creará automáticamente las particiones necesarias.
    
.. figure:: imagenes/inst-08.png
    :width: 600px
    :align: center

.. figure:: imagenes/inst-09.png
    :width: 600px
    :align: center

.. figure:: imagenes/inst-10.png
    :width: 600px
    :align: center

.. figure:: imagenes/inst-11.png
    :width: 600px
    :align: center
    
.. figure:: imagenes/inst-12.png
    :width: 600px
    :align: center

.. figure:: imagenes/inst-13.png
    :width: 600px
    :align: center

.. figure:: imagenes/inst-14.png
    :width: 600px
    :align: center

.. figure:: imagenes/inst-15.png
    :width: 600px
    :align: center5

.. figure:: imagenes/inst-16.png
    :width: 600px
    :align: center

.. warning::
	Preste especial atención a la lista de dispositivos que se muestran, y elija correctamente dónde se instalará del gestor de arranque GRUB.
    
.. figure:: imagenes/inst-17.png
    :width: 600px
    :align: center

.. figure:: imagenes/inst-18.png
    :width: 600px
    :align: center

.. figure:: imagenes/inst-19.png
    :width: 600px
    :align: center

.. figure:: imagenes/inst-20.png
    :width: 600px
    :align: center
    
.. figure:: imagenes/inst-21.png
    :width: 600px
    :align: center
    
.. figure:: imagenes/inst-22.png
    :width: 600px
    :align: center
