Preparar un pendrive USB de arranque
====================================



En Windows y en Linux
`````````````````````

Descargue el programa `balena Etcher <https://www.balena.io/etcher/>`_, instálelo y ejecútelo.

.. figure:: imagenes/etcher.png
    :width: 500px
    :align: center

    Usando balena Etcher

Haga click en  :guilabel:`Select image` y seleccione la imagen ISO.

Haga click en   :guilabel:`Select drive` y seleccione su pendrive USB.

Haga click en   :guilabel:`Flash!`.

.. note::
    Para instalar balena Etcher en Ubuntu o RedHat, vea esta página: <https://github.com/balena-io/etcher#debian-and-ubuntu-based-package-repository-gnulinux-x86x64>`_.    

