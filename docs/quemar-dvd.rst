Preparar un DVD de arranque
===========================


Para instalar GobMis GNU Linux en una computadora, debe preparar un medio de arranque del sistema.


En Linux
````````
Instale y use alguno de los siguientes programas: ``xfburn``, ``brasero`` o ``k3b``.


En Windows
````````
Haga click derecho en el archivo ISO y seleccione Grabar imagen de disco.

Para comprobar que el archivo ISO se grabó sin errores, seleccione Verificar disco después de la grabación.

