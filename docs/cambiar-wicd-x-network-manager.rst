Cambiar el controlador de red Wicd por Network-Manager
======================================================

01 - Desde el menú principal ir a **Centro de control -> Aplicaciones de inicio** y ..

Habilitar item "Red" y deshabilitar item "Wicd"

.. image:: imagenes/aplicaciones-inicio.png
      :align: center


02 - Abrir una terminal y como usuario root  desactivar del inicio el servicio wicd y activar al inicio servicio network-manager ejecutando...

.. code-block:: console

    update-rc.d -f wicd disable && update-rc.d -f network-manager enable

03 - Reiniciar el equipo
