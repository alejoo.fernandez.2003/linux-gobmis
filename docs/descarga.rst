Descarga del archivo imagen
===========================

Para descargar la última versión de GobMis GNU/Linux diríjase al sitio del proyecto `<http://distro.misiones.gob.ar/#descarga>`_.

El archivo ISO de la imagen mide aproximadamente 1,6 Gb.

.. tip::
Una vez que descargue localmente la imagen, le recomendamos que haga una verificación de integridad del archivo que ha descargado, bajando también la **firma digital del archivo ISO** desde la dirección: `<http://distro.misiones.gob.ar/#descarga>`_..

¿ Sólo 64-bits?
---------------

GobMis GNU/Linux está disponible para procesadores con arquitecturas de 64 bits. Considerando que las principales distribuciones han comenzado a discontunuar el soporte para los antiguos procesadores de 32 bits, por el momento no generaremos imagenes ISO para procesadores de 32 bits.

