  

Controladores para conectar las impresoras 
==========================================


En la siguiente tabla encontrará los enlaces para descargar los controladores según sea la marca de la impersora.

+----------+----------------------------+
| Marca    | URL de soporte             |
+==========+============================+
| Brother  |https://bit.ly/3oHWJIk      |
+----------+----------------------------+
| EPSON    |https://bit.ly/3GBfqmY      |
+----------+----------------------------+
| Kyocera  |https://bit.ly/3yfmJxI      |
+----------+----------------------------+
| Xerox    |https://xerox.bz/3dJVZMm    |
+----------+----------------------------+
| Canon    |https://bit.ly/3rXvwDA      |
+----------+----------------------------+
| Lexmark  |https://bit.ly/3yf2gck      |
+----------+----------------------------+
| HPLIP    |https://bit.ly/3dEOmqA      |
+----------+----------------------------+



Impresoras EPSON Chorro de Tinta y Multifunción
------------------------------------------------

Las impresoras de marca EPSON requieren que esté instalado el Sistema Base Standar de Linux (Linux Standard Base - LSB) para poder agregar una impresaora de esa marca.

Desafortunadamente, algunas dependencias necesarias para la instalación, ya no están en los repositorios de la versión 10 de Debían.

Como primer paso, intente instalar los paquetes desde los repositorios con los siguientes comandos.

Abra una terminal pulsando las teclas :guilabel:`Alt+Ctrl+T`.

.. code-block:: console

    su -
    apt-get install lsb
    apt-get install printer-driver-escpr
    apt-get epson-inkjet-printer-escpr

Si este proceso no funcionara, deberá descargar e instalar los paquetes manualmente.

Para cumplir con las dependencias, antes de instalar el controlador específico de la impresora, deberá descargar e instalar manualmente y en el orden que se muestran los siguientes paquetes:

1) *lsb-release*
http://ftp.br.debian.org/debian/pool/main/l/lsb/lsb-release_10.2019051400_all.deb

2) *lsb-invalid-mta*
http://ftp.br.debian.org/debian/pool/main/l/lsb/lsb-invalid-mta_4.1+Debian13+nmu1_all.deb

3) *lsb-base*
http://ftp.br.debian.org/debian/pool/main/l/lsb/lsb-base_4.1+Debian13+nmu1_all.deb

4) *lsb-core*
http://ftp.us.debian.org/debian/pool/main/l/lsb/lsb-core_4.1+Debian13+nmu1_amd64.deb

5) *lsb-printing*
http://ftp.br.debian.org/debian/pool/main/l/lsb/lsb-printing_4.1+Debian13+nmu1_amd64.deb

6) *lsb-compat*
http://ftp.br.debian.org/debian/pool/main/l/lsb/lsb-compat_9.20161125_amd64.deb


Abra una terminal y ejecute los comandos:

.. code-block:: console

    su -
    
    cd ~/Descargas/
    wget http://ftp.br.debian.org/debian/pool/main/l/lsb/lsb-release_10.2019051400_all.deb
    wget http://ftp.br.debian.org/debian/pool/main/l/lsb/lsb-invalid-mta_4.1+Debian13+nmu1_all.deb
    wget http://ftp.br.debian.org/debian/pool/main/l/lsb/lsb-base_4.1+Debian13+nmu1_all.deb
    wget http://ftp.us.debian.org/debian/pool/main/l/lsb/lsb-core_4.1+Debian13+nmu1_amd64.deb
    wget http://ftp.br.debian.org/debian/pool/main/l/lsb/lsb-printing_4.1+Debian13+nmu1_amd64.deb
    wget http://ftp.br.debian.org/debian/pool/main/l/lsb/lsb-compat_9.20161125_amd64.deb
    
    dpkg -i lsb-release_10.2019051400_all.deb
    dpkg -i lsb-invalid-mta_4.1+Debian13+nmu1_all.deb
    dpkg -i lsb-base_4.1+Debian13+nmu1_all.deb
    dpkg -i lsb-core_4.1+Debian13+nmu1_amd64.deb
    dpkg -i lsb-printing_4.1+Debian13+nmu1_amd64.deb
    dpkg -i lsb-compat_9.20161125_amd64.deb
    
A continuación, descargue e instale los controladores de la impresora


.. code-block:: console

    wget https://download3.ebz.epson.net/dsc/f/03/00/10/73/17/307ed265941f8a9a13b826dfe89e5ebc1caa546f/epson-printer-utility_1.1.1-1lsb3.2_amd64.deb
    wget http://download.ebz.epson.net/dsc/f/01/00/01/87/87/b90c68a7844fff22a3f4b4c273fcf3407699f89c/epson-inkjet-printer-201207w_1.0.0-1lsb3.2_amd64.deb

    dpkg -i epson-inkjet-printer-201207w_1.0.0-1lsb3.2_amd64.deb   
    dpkg -i epson-printer-utility_1.1.1-1lsb3.2_amd64.deb



    
    
