Probar sin Instalar
===================

Si no desea instalar el sistema y tan sólo quiere probar la funcionalidad de las aplicaciones, elija la opción **Probar GobMis**.

Esta opción crea una imagen funcional del sistema en la mamoria RAM de la computadora sin alterar ningún dato del disco rígido.

Tenga presente que esta opción demora algo de tiempo mientras se genera la imagen en la memoria. Tenga paciencia ...

Si decide proceder a instalar GobMis, reinicie la computadora, arranque el sistema desde el medio extraible  y luego seleccione la opción **Instalar GobMis**

.. figure:: imagenes/modo-vivo-01.png
    :width: 600px
    :align: center

.. figure:: imagenes/modo-vivo-02.png
    :width: 600px
    :align: center

.. figure:: imagenes/modo-vivo-03.png
    :width: 600px
    :align: center


