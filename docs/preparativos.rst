Preparativos Previos
====================

Antes del iniciar el proceso de instalación en una máquina en uso, deberá seguir un *protocolo* para asegurar la correcta preservación de los archivos de los usuarios.

**Requisitos:** Pendrive, Disco Extreno, Red local


Antes de iniciar cualquier acción, asegúrese de tener y **documentar** las respuestas al siguiente **Cuestionario de Contol**

- *¿Cuántos usuarios utilizan la computadora?*

- *¿Qué nombre de usuarios utilizan?*

- *¿Tienen resguardadas las contraseñas de acceso?*

- *¿Qué aplicaciones específicas están instaladas?. ¿Hay un remplazo disponible en GNU/Linux?* 

- *¿Se almacenan archivos en alguna ubicación particular? ¿Dónde?*

- *¿El disco tiene varias particiones?*


En una máquina con sistema operativo Windows
--------------------------------------------

¿Dónde están los archivos de los usuarios en las distintas versiones de Windows?

- En la layoría de las máquinas con sistema operativo Windows, los archivos de los usuarios se almacenan en la carpeta:  **users**


*Preservación de los archivos existentes*


.. toctree::
   :maxdepth: 1
   :caption: Resguardo de los arhivos del usuario de Windows
   
Use un pendrive un disco externo o una unidad lógica en la red local con suficiente capacidad de almacenamiento.

Copie el contenido de la carepeta **users** del disco al dispositivo externo. Esto puede llevar mucho tiempo dependiendo de la cantidad y tamaño de los archivos a copiar.

Los archivos ocultos: Si copia los archivos ocultos, probablemente incluya a los archivos temporales de las aplicaciones y va a incrementar sustancialmente la cantidad y tamaño de la copia.

En una máquina con sistema operativo GNU/Linux
--------------------------------------------

- En las máquinas con sistema operativo Linux, los archivos de los usuarios se almacenan en la carpeta:  **home**.

-Copie el contenido de la carepeta **home** del disco al dispositivo externo. Esto puede llevar mucho tiempo dependiendo de la cantidad y tamaño de los archivos a copiar.

.. warning::
	Si copia las carpetas de varios usuarios, luego de reinstalar los archivos en el disco deberá reasignar los permisos de acceso de cada usuario con su correspondiente carpeta.




.. toctree::
   :maxdepth: 1
   :caption: Resguardo de los arhivos del usuario de GNU/Linux
   




