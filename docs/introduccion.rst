###############################
Introducción a GobMis GNU/Linux
###############################

.. figure:: imagenes/LogoGobMisTransparente.png
    :width: 200px
    :align: center

*************
Qué es GobMis
*************

Con la visión de dotar a las oficinas del gobierno de la Provincia de Misiones con herramientas y software de gestión de avanzada, creamos una distribución del sistema operativo GNU/Linux, adaptada a los requerimientos de una administración contemporánea, incorporando herramientas de Software Libre.

Basado en el Sistema Operativo **Devuan (Debian)**, esta distribución contiene herramientas de uso habitual en las oficinas gubernamentales, cubriendo todas las necesidades informáticas de la administración pública.



**Software Preinstalado**
=========================


**Accesorios:**
---------------

* ``balenaEtcher`` Comúnmente conocido como Etcher, es una utilidad gratuita y de código abierto que se utiliza para escribir archivos de imagen como archivos .iso e .img, así como carpetas comprimidas en medios de almacenamiento para crear tarjetas SD en vivo y unidades flash USB. 

* ``Calculadora``  Una calculadora simple

* ``Capturar pantalla``

* ``Nextcloud`` Es un servicio de alojamiento de archivos. Su funcionalidad es similar al software Dropbox, aunque Nextcloud es de tipo código abierto, permitiendo a quien lo desee instalarlo en un servidor privado. Su arquitectura abierta permite añadir funcionalidad al servidor en forma de aplicaciones. Se integra con la nube institucional del gobierno de Misiones: https://arai.misiones.gob.ar

* ``Copias de respaldo`` Una utilidad para automatizar el proceso de resguardo de archivos del usuario.

* ``Engrampa`` Es un administrador de archivos comprimidos.

* ``GtkHash`` Es una utilidad de escritorio para calcular resúmenes de mensajes o sumas de verificación. Se admiten las funciones hash más conocidas, incluidas MD5, SHA1, SHA2 (SHA256 / SHA512), SHA3 y BLAKE2.

* ``Herramienta de búsqueda de MATE``

* ``KeePassXC`` Es un administrador de contraseñas de software libre y de código abierto.

* ``Gnome Mapas`` Es un mapa abierto y colaborativo basado en OpenStreetMap

* ``Midnight Commander`` Es un gestor de archivos ortodoxo para sistemas tipo Unix y es un clon del Norton Commander. Es una aplicación que funciona en modo texto. La pantalla principal consiste en dos paneles en los cuales se muestra el sistema de archivos.

* ``Pluma`` Es un editor de textos

* ``Stacer`` Es un monitor de aplicaciones y optimizador de sistemas de código abierto que ayuda a los usuarios a administrar todo el sistema con diferentes aspectos, es una utilidad de sistema todo en uno.

* ``Synapse``

* ``Visor de tipografías``

**Oficina:**
------------


* ``calibre`` Administrador y lector de documentos e-book en formato pub  

* ``Diccionario``

* ``E-book Editor`` Editor de documentos e-book.

* ``Planner`` Gestor de proyectos 

* ``HPLIP Fax Utility``

* ``LibreOffice`` Suite ofimática libre.

* ``Osmo`` Un organizador de información personal compacto pero rico en funciones para Linux, se distingue del paquete de otras aplicaciones de calendario debido a su diseño liviano y fácil de usar. El Osmo incluye un calendario de agenda, un organizador de tareas pendientes y una lista de contactos, todo con muchas opciones intuitivas.

* ``PDF Arranger`` Editor de documentos PDF que permite reorganizar ohoas y unir documentos. 

* ``Evince`` Visor de documentos en formato PDF


**Internet:**	
-------------

* ``AnyDesk``  Es un programa de escritorio remoto.​ Provee acceso remoto bidireccional entre computadoras personales y está disponible para todos los sistemas operativos comunes. 

* ``Thunderbird`` Es un cliente de correo electrónico multiplataforma, libre y de código abierto, cliente de noticias, cliente de RSS y de chat desarrollado por la Fundación Mozilla.

* ``Gajim`` Es un cliente de software libre de mensajería instantánea para Jabber/XMPP que hace uso del toolkit GTK.

* ``iptux`` Es un cliente de mensajería IP para Linux. Detecta automáticamente otros clientes en la intranet, envía mensajes a otros clientes y envía archivos a otros clientes.

* ``Pidgin``  Es un programa de chat que le permite iniciar sesión en cuentas en múltiples redes de chat simultáneamente. Esto significa que puede chatear con amigos en XMPP y participar en un canal de IRC al mismo tiempo.

* ``Mumble`` Es una aplicación multiplataforma libre de voz sobre IP especializada en la multiconferencia. Usa una arquitectura cliente-servidor donde los usuarios que quieren hablar se conectan al mismo servidor.

* ``Chromium``  Es un navegador web de código abierto desarrollado por Google. En términos generales, es un proyecto paralelo de Google Chrome.

* ``Firefox ESR``  Es una versión oficial de Firefox desarrollada para grandes organizaciones como universidades y empresas que necesitan configurar y mantener Firefox a gran escala. Firefox ESR no incluye las funciones más recientes, pero tiene las últimas correcciones de seguridad y estabilidad.

* ``Telegram de Escritorio`` Es una aplicación de escritorio que permite hablar con todos loss contactos de este servicio de mensajería instantánea de una forma cómoda y sencilla, directamente desde la computadora.

* ``Wicd`` Es un administrador de redes cableadas e inalámbricas de código abierto para Linux que tiene como objetivo proporcionar una interfaz simple para conectarse a redes con una amplia variedad de configuraciones. 

* ``EX2Go``  Es una solución para acceder de forma remota a aplicaciones y escritorios basados en X11 de manera más eficiente que utilizando el reenvío X11 estándar. El servidor X2Go está disponible para Linux y el cliente está disponible para todos los principales sistemas operativos de escritorio.

**Gráficos:**	
-------------

* ``Dia`` Es un software de dibujo de código abierto gratuito. Dia admite más de 30 tipos de diagramas diferentes, como diagramas de flujo, diagramas de red, modelos de bases de datos. Más de mil objetos prefabricados ayudan a dibujar diagramas profesionales. Dia puede leer y escribir varios formatos de imágenes vectoriales y de trama diferentes. Los desarrolladores de software y los especialistas en bases de datos pueden usar Dia como una herramienta CASE para generar esqueletos de código a partir de sus dibujos.

* ``Inkscape`` Es un editor de gráficos vectoriales. Inkscape puede crear y editar diagramas, líneas, gráficos, logotipos, e ilustraciones complejas. El formato principal que utiliza el programa es Scalable Vector Graphics.

* ``ImageMagick``  Es un conjunto de utilidades para mostrar, manipular y convertir imágenes, capaz de leer y escribir más de 200 formatos.


* ``GIMP`` Es un editor de imágenes multiplataforma. 

* ``Scribus`` Scribus es un programa de maquetación de páginas, creado para el diseño de publicaciones, la composición tipográfica y la preparación de archivos para equipos de configuración de imágenes de calidad profesional mediante computadora y se encuentra disponible en 24 idiomas.

* ``Evince`` Es un visor de documentos para el entorno de escritorio GNOME. Se pueden ver los archivos en formato PDF, PostScript y DjVu. La meta de Evince es reemplazar, con una simple aplicación, los múltiples visores de documentos que existen.

* ``EOM`` O el Ojo de MATE es el visor de imágenes oficial para el entorno de escritorio MATE. Su objetivo es ser simplista, liviano y fácil de usar. EOM también se puede utilizar para realizar tareas básicas como voltear y rotar imágenes.

* ``XSane`` Es una aplicación que le permite controlar un escáner y adquirir imágenes de él. Con XSane, puede fotocopiar documentos fácilmente y guardar, enviar por fax o correo electrónico sus imágenes escaneadas. Incluso puede guardar sus escaneos como documentos de varias páginas, en lugar de archivos separados de una página. XSane también se puede utilizar desde el GIMP.

**Sonido y Video:**
-------------------


* ``Clementine`` Es un reproductor de audio, admite una gran cantidad de formatos de audio. Tiene un software de administrador de música incorporado. Admite escuchar radio en línea, capaz de copiar música en iPod, iPhone, reproductor USB. 

* ``Cheese`` Cheese es un programa que le permite tomar fotos, videos y cualquier otra cosa que se le ocurra con su cámara web. 

* ``Brasero`` Es una aplicación para grabar CD o DVD. Está diseñado para ser lo más simple posible y tiene algunas características únicas que permiten a los usuarios crear sus discos de manera fácil y rápida.

* ``VLC`` Es un reproductor multimedia multiplataforma y un entorno que reproduce la mayoría de archivos multimedia, así como DVD, Audio CD, VCD y diversos protocolos de transmisión.


**Herramientas del Sistema:**
-----------------------------


* ``Analizador de uso de disco`` 

* ``BleachBit`` Es una herramienta de limpieza del disco duro de software libre y de código abierto, administrador de privacidad y optimizador del sistema. 

* ``Caja`` Es un administrador de archivos.

* ``GParted`` Es un editor de particiones del disco. Esta aplicación es usada para crear, eliminar, redimensionar, inspeccionar y copiar particiones, como también los sistemas de archivos que se encuentran en ellas.

* ``GRsync`` Es una interfaz gráfica de usuario para Rsync. Una herramienta de sincronización de archivos y copia de seguridad diferencial ampliamente utilizada en sistemas operativos similares a Unix. 

* ``Htop`` Es un visor y administrador de procesos del monitor del sistema interactivo. Está diseñado como una alternativa a la parte superior del programa Unix. Muestra una lista actualizada con frecuencia de los procesos que se ejecutan en una computadora, normalmente ordenados por la cantidad de uso de la CPU.

* ``Hardinfo`` Abreviado para "información de hardware", es un perfilador de sistemas y una herramienta gráfica de referencia para sistemas Linux, que es capaz de recopilar información tanto del hardware como de algún software y organizarla en una herramienta GUI fácil de usar. Puede mostrar información sobre estos componentes: CPU, GPU, placa base, RAM, almacenamiento, disco duro, impresoras, puntos de referencia, sonido, red y USB, así como información del sistema como el nombre de la distribución, la versión y la información del kernel de Linux.

* ``Prueba de velocidad (speedtest.py)`` 

* ``Terminal de MATE`` Una interfaz de comandos del sistema

**Otros:**
----------

* ``Descifrar archivos`` 

* ``Dukto`` Es una herramienta sencilla de transferencia de archivos diseñada para uso en LAN. Transfiera archivos de una PC (u otro dispositivo) a otra, sin preocuparse por los usuarios, permisos, sistemas operativos, protocolos, clientes, servidores, etc. Simplemente inicie Dukto en las dos PC y transfiera archivos y carpetas arrastrándolos a la ventana.
