Drivers para activar los adaptadores Wi-Fi
==========================================

Para identificar el dispositivo y controlador instalado ejecute el siguiente comando:

**inxi -n**

Obtendrá como resultado algo similar a lo siguiente:

Network:
  Device-1: Qualcomm **Atheros** AR9271 802.11n type: USB driver: **ath9k_htc**
  IF: wlx6470020db74c state: down mac: 64:70:02:0d:b7:4c 
  IF: eno1 state: up speed: 100 Mbps duplex: full mac: 80:ce:62:18:1c:c2 
  
  Device-2: Intel Dual Band Wireless-AC 3168NGW [Stone Peak] driver: iwlwifi 
  IF: wlo1 state: up mac: 40:a3:cc:a1:9d:57 

En base al resultado de la consulta, vea en la siguiente tabla el nombre del fabricante y el paquete asociado que contiene los controladores.

+----------+-------------------------------------------------------------------------------------------------------+
| Marca    | Paquete                                                                                               |
+==========+=======================================================================================================+
| Atheros  |firmware-atheros                                                                                       |
|          |https://debian.pkgs.org/11/debian-nonfree-amd64/firmware-atheros_20210315-3_all.deb.html               |
+----------+-------------------------------------------------------------------------------------------------------+
| Broadcom |firmware-b43-installer, broadcom-sta-dkms, firmware-b43legacy-installer o firmware-brcm80211           |
+----------+-------------------------------------------------------------------------------------------------------+
| Intel    |Firmware-ipw2x00, firmware-intelwimax o firmware-iwlwifi                                               |
+----------+-------------------------------------------------------------------------------------------------------+
| Realtek  |firmware-realtek                                                                                       |
|          |https://debian.pkgs.org/11/debian-nonfree-amd64/firmware-realtek_20210315-3_all.deb.html               |
+----------+-------------------------------------------------------------------------------------------------------+
| ti       |firmware-ti-connectivity                                                                               |
+----------+-------------------------------------------------------------------------------------------------------+
| ZyDas    |firmware-zd1211                                                                                        |
+----------+-------------------------------------------------------------------------------------------------------+
| Ralink   |firmware-ralink                                                                                        |
+----------+-------------------------------------------------------------------------------------------------------+
| RT-Link  |firmware-ath9k-htc                                                                                     |
|          |https://debian.pkgs.org/11/debian-main-amd64/firmware-ath9k-htc_1.4.0-106-gc583009+dfsg1-1_all.deb.html|
+----------+-------------------------------------------------------------------------------------------------------+


En el caso que use una tarjeta de *Atheros*, proceder a la instalación del paquete firmware-atheros ejecutando el siguiente comando en la terminal:

**sudo apt-get install firmware-atheros**


.. note::
	En caso de que no se halle el paquete en el repositorio, o no tenga conectividad a Intertet, deberá previamente descargar el paquete desde el enlace que se muestra en la tabla más arriba, copiar el paquete en una carpeta de la máquida que está instalando, y proceder al instalar manualmente desde una ventana de la terminal.
	Ejecute el comando **sudo dpkg -i  firmware**-:guilabel:`tab` 



Si además desea confirmar que el chip está soportado por el driver que ofrece Debian, accediendo al gestor de paquetes tal y como se puede ver a continuación, la descripción de los paquetes ofrece información detallada de los chips que soporta cada uno de los drivers.

.. image:: imagenes/synaptic-drivers-wifi.png
      :align: center

Si después de seguir las instrucciones aún persisten los problemas para conectarse vía Wi-Fi, se recomienda que abrir una terminal y ejecutar el comando:

**sudo apt-get install firmware-linux firmware-linux-free firmware-linux-nonfree firmware-misc-nonfree**

Una vez finalizada la instalación de los paquetes se deberá reiniciar la computadora.

Instalar controladores Realtek RTL8723DE (D723)
===============================================

RTL8723DE en GobMis 2.0

IMPORTANTE: Loguearse como root

0) Actualizar el sistema...

.. code-block:: console

    apt update && apt full-upgrade -y

1) Desinstalar firmware-realtek...

.. code-block:: console

    apt remove --purge firmware-realtek

2) Agregar repositorio kernel liquorix

2.1) Agregar llave del repositorio:

.. code-block:: console

    curl https://liquorix.net/linux-liquorix.pub | apt-key add - 


2.2) Crear archivo para el repositorio:

.. code-block:: console

    echo -e "## Liquorix\ndeb http://liquorix.net/debian buster main\n#deb-src http://liquorix.net/debian buster main" > /etc/apt/sources.list.d/liquorix.list

3) Actualizar repositorio:

.. code-block:: console

    apt update

4) Instalar kernel liquorix:

.. code-block:: console

    apt install dkms git linux-image-liquorix-amd64 linux-headers-liquorix-amd64

5) Reiniciar:

.. code-block:: console

    reboot

6) Compilar e instalar módulo vía DKMS (https://github.com/smlinux/rtl8723de)

.. code-block:: console

    cd Descargas
    git clone https://github.com/smlinux/rtl8723de.git -b current
    dkms add ./rtl8723de
    dkms install rtl8723de/5.1.1.8_21285.20171026_COEX20170111-1414
    depmod -a
    reboot

7) Reemplazar el gestor de redes Wicd por NetworkManager...

.. code-block:: console

    update-rc.d -f wicd disable && update-rc.d -f network-manager enable

8) Reiniciar e ingresar a la sesión del usuario


9) Configurar ejemplo en escritorio MATE:

Seleccionar el el menú del entorno gráfico 

Sistema -> Centro de control -> Aplicaciones al inicio (Para el escritorio MATE)

9.1) Marcar la aplicación "Red"

9.2) Desmarcar marcar la aplicación "Wicd Network Manager Tray"

9.3) Cerrar

10) Reiniciar

